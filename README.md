#### Basic reusable components for machine learning tasks
###### Reusable Functions for:
<ol>
  <li>data cleaning,</li>
  <li>data preparations,</li>
  <li>model selection,</li>
  <li>model tuning.</li>

